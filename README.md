# Exercice 2 

---

Ecrire un programme dans lequel l’utilisateur donne un entier et le programme lui renvoie un client

Un client est définit par 
- un nom
- une adresse
    - nomRue
    - numéroRue
    - CodePostal
    - Ville
- une adresse mail

Lorsqu'un utilisateur saisie l'url <http://localhost:8080/client/1> cela renvoie le client :

**Martin Labbé**  
83, rue Pierre Motte  
88100 SAINT-DIÉ  
martinLabbe@dayrep.com  

Lorsqu'un utilisateur saisie l'url <http://localhost:8080/client/2> cela renvoie le client :

**Emmeline Rhéaume**   
95, rue Charles Corbeau  
27000 ÉVREUX    
EmmelineRheaume@dayrep.com 