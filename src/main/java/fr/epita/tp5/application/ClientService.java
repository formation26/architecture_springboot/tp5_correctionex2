package fr.epita.tp5.application;

import fr.epita.tp5.domaine.Client;

public interface ClientService {
	
	Client getClient(int id);

}
