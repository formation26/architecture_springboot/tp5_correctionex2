package fr.epita.tp5.application;

import org.springframework.stereotype.Service;

import fr.epita.tp5.domaine.Adresse;
import fr.epita.tp5.domaine.Client;

@Service
public class ClientServiceImpl implements ClientService {

	@Override
	public Client getClient(int id) {

		Client c;
		switch (id) {

		case 1:
			c = generateClient1();
			break;

		case 2:
			c = generateClient2();

			break;

		default:
			c = null;
		}

		return c;
	}

	private Client generateClient1() {

		Adresse adresse = new Adresse("rue Pierre Motte", 83, "88100", "SAINT-DIÉ");
		Client c = new Client("Martin Labbé", adresse, "martinLabbe@dayrep.com");
		return c;
	}

	private Client generateClient2() {

		Adresse adresse = new Adresse("rue Charles Corbeau", 95, "27000", "ÉVREUX");
		Client c = new Client("Emmeline Rhéaume", adresse, "EmmelineRheaume@dayrep.com");
		return c;
	}

}
