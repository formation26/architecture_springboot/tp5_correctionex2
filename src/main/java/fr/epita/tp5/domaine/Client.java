package fr.epita.tp5.domaine;

public class Client {
	private String nom;
	private Adresse adresse;
	private String email;
	
	
	public Client(String nom, Adresse adresse, String email) {
		
		this.nom = nom;
		this.adresse = adresse;
		this.email = email;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public Adresse getAdresse() {
		return adresse;
	}


	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}
	
	
	
	

}
