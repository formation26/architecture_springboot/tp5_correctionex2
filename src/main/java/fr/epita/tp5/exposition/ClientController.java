package fr.epita.tp5.exposition;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.epita.tp5.application.ClientService;
import fr.epita.tp5.domaine.Client;

@RestController
@RequestMapping("/client")
public class ClientController {
	@Autowired
	ClientService service;
	
	
	@GetMapping("{id}")
	public Client getClient(@PathVariable("id") int id) {
		
		
		return service.getClient(id);
	}

}
